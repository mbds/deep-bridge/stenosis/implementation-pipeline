# Module de lecture d'images pour la pipeline

La pipeline comporte plusieurs modules de traitements différents :
- l'import des données
- le prétraitement
- la segmentation
- la classification

Les données prises en entrées du premier module peuvent avoir des formats variés. Ce code permet la lecture du format DICOM.

Chaque module transmettra les images sous un format unifié, un tenseur JSON, qui permettra au module suivant d'y appliquer des traitements.

Le code joint à ce projet permet de :
- parcourir un dossier "Test" contenant des images DICOM
- récupérer les metadatas liées à ces dernières
- transformer chaque image DICOM en json
- retourner dans un nouveau fichier "dcm.json" la chaîne de charactères correspondant au json
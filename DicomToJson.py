import pydicom
import os
import json

# On va chercher nos fichiers .dcm et on les met dans listFilesDCM
pathDicom = "./Test/"
listFilesDCM = []
for dirname, subdirList, fileList in os.walk(pathDicom):
	for filename in fileList:
		if ".dcm" in filename.lower():
			listFilesDCM.append(os.path.join(dirname, filename))

# On construit notre JSON à partir de toutes les images DICOM
## METADATAS
output = "{\"meta\": "
try:
	# Étant donné que toutes les slices partagent les mêmes meta-datas,
	# on récupère arbitrairement celles du tout premier élément de la liste
	metaDCM = pydicom.read_file(listFilesDCM[0])
except:
	output = output + "null"
else:
	output = output + metaDCM.to_json()
## DICOMS
output = output + ", \"dicom\":["
intersections = len(listFilesDCM) - 1
for dcm in listFilesDCM:
	ds = pydicom.dcmread(dcm)
	output = output + ds.to_json()
	if (intersections > 0):
		output = output + ","
		intersections = intersections - 1
output = output + ']}'

# On écrit dans un fichier JSON
with open("dcm.json", 'w') as outfile:
	json.dump(output, outfile, ensure_ascii=False)
